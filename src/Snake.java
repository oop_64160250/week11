public class Snake extends Animal implements Crawable, Swimable {
    
    public Snake(String name) {
        super(name, 2);
    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");
        
    }

    @Override
    public void craw() {
        System.out.println(this + " craw.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");

        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
        
    }

    @Override
    public String toString() {
        return "snake(" + getName()+")"; 
    }

}
