public class Bus extends Vehicle implements Walkable{

    public Bus(String name, String engine) {
        super(name, engine);

    }

    @Override
    public void walk() {
        System.out.println(this + " walk. ");
        
    }

    @Override
    public void run() {
        System.out.println(this + " run. ");
        
    }

    @Override
    public String toString() {
        return "bus(" + getName()+")"; 
    }
}
