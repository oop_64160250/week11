public class Submarine extends Vehicle implements Swimable{

    public Submarine(String name, String engine) {
        super(name, engine);
    } 

    @Override
    public void swim() {
        System.out.println(this + " swim.");
        
    }
    
    @Override
    public String toString() {
        return "submarine(" + getName()+")"; 
    }
}
