public class App {
    public static void main(String[] args ) 
    {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        Plane boeing = new Plane("Boeing", "Rosaroi") ;
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("Clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        Submarine prayuth = new Submarine("Prayuth", "Rosaroi");
        prayuth.swim();
        Bus tour = new Bus("tour", "Benz");
        tour.walk();
        Snake mamba = new Snake("Mamba");
        mamba.eat();
        mamba.sleep();
        mamba.swim();
        mamba.craw();
        Rat rat1 = new Rat("Cute");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        Dog dog1 = new Dog("Plato");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        dog1.swim();
        Cat cat1 = new Cat("Valen");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        Flyable[] flyables = {bird1, boeing, clark};
        for(int i=0; i<flyables.length;i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        Walkable[] walkables = {bird1, clark, man1};
        for(int i=0; i<walkables.length;i++) {
            walkables[i].walk();
            walkables[i].run();
            
        }
    }
}
